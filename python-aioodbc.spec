%global _empty_manifest_terminate_build 0
Name:		python-aioodbc
Version:	0.4.0
Release:	2
Summary:	ODBC driver for asyncio.
License:	Apache-2.0
URL:		https://github.com/aio-libs/aioodbc
Source0:	https://files.pythonhosted.org/packages/30/65/1c285bd56b32b53eb30c90712c2398d7012c1f28e9effc056fb2ed1f6543/aioodbc-0.4.0.tar.gz
BuildArch:	noarch


%description
**aioodbc** is a Python 3.5+ module that makes it possible to access ODBC_ databases
with asyncio_. 

%package -n python3-aioodbc
Summary:	ODBC driver for asyncio.
Provides:	python-aioodbc
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-aioodbc
**aioodbc** is a Python 3.5+ module that makes it possible to access ODBC_ databases
with asyncio_. 

%package help
Summary:	Development documents and examples for aioodbc
Provides:	python3-aioodbc-doc
%description help
**aioodbc** is a Python 3.5+ module that makes it possible to access ODBC_ databases
with asyncio_. 

%prep
%autosetup -n aioodbc-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-aioodbc -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Fri Mar 15 2024 lifeifei <lifeifei@kylinos.cn> - 0.4.0-2
- License compliance rectification

* Tue Apr 4 2023 wubijie <wubijie@kylinos.cn> - 0.4.0-1
- Update package to version 0.4.0

* Mon Jul 06 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
